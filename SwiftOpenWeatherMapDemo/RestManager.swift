//
//  RestManager.swift
//  SwiftOpenWeatherMapDemo
//
//  Created by Itcrystal Mac on 25.08.16.
//  Copyright © 2016 Volodymyr Nazarkevych. All rights reserved.
//


import Foundation
import UIKit
import AFNetworking

class RestManager: NSObject {
    static let sharedInstance = RestManager()
    var opManager: AFHTTPSessionManager?
    
    override init() {
        
        let  baseUrl = NSURL.init(string: Constants.BASE_URL)
        opManager = AFHTTPSessionManager.init(baseURL: baseUrl)
        opManager?.requestSerializer = AFJSONRequestSerializer.init()
        
        opManager?.responseSerializer = AFJSONResponseSerializer.init(readingOptions: NSJSONReadingOptions.AllowFragments)
        opManager?.securityPolicy = AFSecurityPolicy.init(pinningMode: AFSSLPinningMode.None)
        opManager?.securityPolicy.allowInvalidCertificates = true
        opManager?.securityPolicy.validatesDomainName = false
        super.init()
    }
    
}


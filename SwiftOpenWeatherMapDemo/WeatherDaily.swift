//
//  WeatherDaily.swift
//  SwiftOpenWeatherMapDemo
//
//  Created by Itcrystal Mac on 26.08.16.
//  Copyright © 2016 Volodymyr Nazarkevych. All rights reserved.
//

import Foundation
import SwiftyJSON

struct WeatherDaily {
    var dateTime: String
    var description: String
    var temperature: String
    var minTemp: String
    var maxTemp: String
    var imageName: String
    
    init(json: JSON) {
        self.dateTime = json["dt"].doubleValue.convertTimeToStringWithoutHours()
        self.description = json["weather"][0]["description"].stringValue
        self.temperature = String(json["temp"]["eve"].intValue)
        self.imageName = json["weather"][0]["icon"].stringValue
        self.minTemp = String(json["temp"]["min"].intValue)
        self.maxTemp = String(json["temp"]["max"].intValue)
    }
}

extension Double {
    func convertTimeToStringWithoutHours() -> String{
        let currentDateTime = NSDate(timeIntervalSince1970: self)
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/MM"
        return dateFormatter.stringFromDate(currentDateTime)
    }
}
//
//  Weather.swift
//  SwiftOpenWeatherMapDemo
//
//  Created by Itcrystal Mac on 26.08.16.
//  Copyright © 2016 Volodymyr Nazarkevych. All rights reserved.
//

import Foundation

import SwiftyJSON

struct Weather {
    var dateTime: String
    var description: String
    var temperature: String
    var minTemp: String
    var maxTemp: String
    var imageName: String
    
    init(json: JSON) {
        self.dateTime = json["dt"].doubleValue.convertTimeToString()
        self.description = json["weather"][0]["description"].stringValue
        self.temperature = String(json["main"]["temp"].intValue)
        self.minTemp = String(json["main"]["temp_min"].intValue)
        self.maxTemp = String(json["main"]["temp_max"].intValue)
        self.imageName = json["weather"][0]["icon"].stringValue
    }
}



extension Double {
    func convertTimeToString() -> String{
        let currentDateTime = NSDate(timeIntervalSince1970: self)
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/MM hh:mm"
        return dateFormatter.stringFromDate(currentDateTime)
    }
}
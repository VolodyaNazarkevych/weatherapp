//
//  HourlyTableViewCell.swift
//  SwiftOpenWeatherMapDemo
//
//  Created by Itcrystal Mac on 25.08.16.
//  Copyright © 2016 Volodymyr Nazarkevych. All rights reserved.
//

import UIKit

class HourlyTableViewCell: UITableViewCell {

    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var time: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected == true {
            let color = CGColorCreateCopyWithAlpha(UIColor.clearColor().CGColor, 0.4)
            backgroundColor = UIColor.init(CGColor: color!)
        } else {
            let color = CGColorCreateCopyWithAlpha(UIColor.clearColor().CGColor, 0.0)
            backgroundColor = UIColor.init(CGColor: color!)
        }
        // Configure the view for the selected state
    }
    
}

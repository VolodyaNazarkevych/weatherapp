//
//  ViewController.swift
//  SwiftOpenWeatherMapDemo
//
//  Created by Itcrystal Mac on 25.08.16.
//  Copyright © 2016 Volodymyr Nazarkevych. All rights reserved.
//

import UIKit
import SwiftOpenWeatherMapAPI
import CoreLocation
import SwiftyJSON

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let weatherAPI = WAPIManager(apiKey: Constants.weatherMapAPIKey, temperatureFormat: .Celsius)
    
    @IBOutlet weak var weatherDescriptionLabel: UILabel!
    @IBOutlet weak var weatherDescriptionIcon: UIImageView!
    @IBOutlet weak var currentTemperature: UILabel!

    @IBOutlet weak var townName: UILabel!
    
    @IBOutlet weak var maxTemperature: UILabel!
    @IBOutlet weak var minTemperature: UILabel!
    
    @IBOutlet weak var forecastTableView: UITableView!
    
    var locationManager: LocationManager?
    var refreshControl: UIRefreshControl!
    
    var currentWeather: Weather? {
        didSet {
            self.currentTemperature.text = (currentWeather?.temperature)! + "°C"
            self.weatherDescriptionLabel.text = currentWeather?.description
            self.maxTemperature.text = (currentWeather?.maxTemp)! + "°C"
            self.minTemperature.text = (currentWeather?.minTemp)! + "°C"
            self.weatherDescriptionIcon.image = UIImage(named: Constants.imageMap().valueForKey((currentWeather?.imageName)!)! as! String)
        }
    }
    
    var weatherList = [Weather]() {
        didSet {
            self.forecastTableView.reloadData()
        }
    }
    
    var weatherDailyList = [WeatherDaily]() {
        didSet {
            self.forecastTableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(ViewController.refresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        forecastTableView.addSubview(refreshControl)
        
        forecastTableView.registerNib(UINib.init(nibName: Constants.hourlyCell, bundle: nil), forCellReuseIdentifier: Constants.hourlyCell)
        forecastTableView.registerNib(UINib.init(nibName: Constants.dailyCell, bundle: nil), forCellReuseIdentifier: Constants.dailyCell)
        
        self.locationManager = LocationManager()
//        let userLocation = CLLocation.init(latitude: 49.85, longitude: 24.0166666667)
        //updateWeatherDataByCoordinates(userLocation)
        self.locationManager!.getlocationForUser { (userLocation: CLLocation) -> () in
            self.updateWeatherDataByCoordinates(userLocation)
        }
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func updateWeatherDataByCoordinates(userLocation: CLLocation) {
        self.weatherAPI.currentWeatherByCoordinatesAsJson(userLocation.coordinate, data: { (WeatherResult) -> Void in
            switch WeatherResult {
            case .Success(let json):
                self.townName.text = json["name"].stringValue
                self.currentWeather = Weather(json: json)
            case .Error(let errorMessage):
                self.showErrorAlert(errorMessage)
                break
            }
        })
        
        self.weatherAPI.dailyForecastWeatherByCoordinatesAsJson(userLocation.coordinate, data: {(WeatherResult) -> Void in
            switch WeatherResult {
            case .Success(let json):
                self.weatherDailyList = json["list"].array!.map() { WeatherDaily(json: $0) }
                break
            case .Error(let errorMessage):
                self.showErrorAlert(errorMessage)
                break
            }
        })
        
        self.weatherAPI.forecastWeatherByCoordinatesAsJson(userLocation.coordinate, data: {(WeatherResult) -> Void in
            switch WeatherResult {
            case .Success(let json):
                self.weatherList = json["list"].array!.map() { Weather(json: $0) }
                break
            case .Error(let errorMessage):
                self.showErrorAlert(errorMessage)
                break
            }
        })
    }
    
    
    //MARK: UITableViewDataSource
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Daily Forecast"
        } else {
            return "Hourly Forecast"
        }
    }
    
    func tableView(tableView: UITableView, willDisplayHeaderView view:UIView, forSection: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.textLabel?.textColor = UIColor.whiteColor()
            headerView.backgroundView?.backgroundColor = UIColor(CGColor: CGColorCreateCopyWithAlpha(UIColor.clearColor().CGColor, 0.5)!)
        }
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(40.0)
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.min
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return weatherDailyList.count
        } else {
            return weatherList.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let weather = weatherDailyList[indexPath.row]
            let cell = forecastTableView.dequeueReusableCellWithIdentifier(Constants.dailyCell, forIndexPath: indexPath) as! DailyTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            
            cell.weatherIcon.image = UIImage(named: Constants.imageMap().valueForKey((weather.imageName))! as! String)
            cell.minMaxTemperature.text = weather.maxTemp + "°C/" + weather.minTemp + "°C"
            cell.dayOfWeek.text = weather.dateTime + "  " + weather.description
            return cell
        } else {
            let weather = weatherList[indexPath.row]
            
            let cell = forecastTableView.dequeueReusableCellWithIdentifier(Constants.hourlyCell, forIndexPath: indexPath) as! HourlyTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            
            cell.weatherIcon.image = UIImage(named: Constants.imageMap().valueForKey((weather.imageName))! as! String)
            cell.temperature.text = weather.temperature + "°C"
            cell.time.text = weather.dateTime
            return cell
        }
    }

    
    
    //MARK: - Action
    
    
    func refresh(sender: AnyObject) {
        self.locationManager!.getlocationForUser { (userLocation: CLLocation) -> () in
            self.updateWeatherDataByCoordinates(userLocation)
        }
        self.forecastTableView.reloadData()
        self.refreshControl?.endRefreshing()
    }

}



extension ViewController {
    private func showErrorAlert(errorMessage: String) {
        let alertController = UIAlertController(title: "Error", message: errorMessage, preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: nil))
        self.presentViewController(alertController, animated: true, completion: nil)
    }
}


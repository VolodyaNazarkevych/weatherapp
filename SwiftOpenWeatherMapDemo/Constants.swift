//
//  Constants.swift
//  SwiftOpenWeatherMapDemo
//
//  Created by Itcrystal Mac on 25.08.16.
//  Copyright © 2016 Volodymyr Nazarkevych. All rights reserved.
//

import Foundation

struct Constants {
    static let weatherMapAPIKey = "4dccdb47b1cf069759431dd669d23c81"
    static let BASE_URL = ""
    static let dailyCell = "DailyTableViewCell"
    static let hourlyCell = "HourlyTableViewCell"
    
    
    static func imageMap() -> NSDictionary {
        let imageMap = [
            "01d" : "weather-clear",
            "02d" : "weather-few",
            "03d" : "weather-few",
            "04d" : "weather-broken",
            "09d" : "weather-shower",
            "10d" : "weather-rain",
            "11d" : "weather-tstorm",
            "13d" : "weather-snow",
            "50d" : "weather-mist",
            "01n" : "weather-moon",
            "02n" : "weather-few-night",
            "03n" : "weather-few-night",
            "04n" : "weather-broken",
            "09n" : "weather-shower",
            "10n" : "weather-rain-night",
            "11n" : "weather-tstorm",
            "13n" : "weather-snow",
            "50n" : "weather-mist",
            ]
        return imageMap
    }}
